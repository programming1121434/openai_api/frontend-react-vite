FROM node:18.14.1 as builder
WORKDIR /app
COPY ["package.json", "package-lock.json", "./"]
RUN npm install
COPY . .
RUN npm run build 
RUN npm prune --production
RUN npm cache clean --force

FROM nginx:1.23.4
WORKDIR /usr/share/nginx/html
COPY --from=builder /app/dist/ .
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]